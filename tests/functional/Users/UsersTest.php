<?php declare(strict_types=1);

namespace App\Tests\Functional\Users;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsersTest extends WebTestCase
{
    private const USERNAME = 'JohnDoe';
    private const EMAIL = 'test.email@gmail.com';
    private const PASSWORD = ",\u0026+3RjwAu88(tyC\u0027";

    protected function setUp(): void
    {
        parent::setUp();

        $this->clearTableUser();
    }

    public function testUserCanSignUp(): void
    {
        $client = self::createClient();
        $client->request('POST', '/users', [], [], [], json_encode([
            'userName' => self::USERNAME,
            'email' =>  self::EMAIL,
            'password' => self::PASSWORD
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        unset($response['id']);
        $expectedResponse = [
            'userName' => self::USERNAME,
            'email' => self::EMAIL,
            'password' => self::PASSWORD,
        ];

        self::assertEquals($expectedResponse, $response);
    }

    public function testUserCannotSignUpTwice(): void
    {
        $client = self::createClient();
        $client->request('POST', '/users', [], [], [], json_encode([
            'userName' => self::USERNAME,
            'email' =>  self::EMAIL,
            'password' => self::PASSWORD
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $client->request('POST', '/users', [], [], [], json_encode([
            'userName' => self::USERNAME,
            'email' =>  self::EMAIL,
            'password' => self::PASSWORD
        ]));
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }

    public function testUserCanBeArchived(): void
    {
        $client = self::createClient();
        $client->request('POST', '/users', [], [], [], json_encode([
            'userName' => self::USERNAME,
            'email' =>  self::EMAIL,
            'password' => self::PASSWORD
        ]));

        $client->request('DELETE', '/users', [], [], [], json_encode([
            'email' =>  self::EMAIL
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    private function clearTableUser(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $connection = $entityManager->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL('user', true));
    }
}
