<?php declare(strict_types=1);

namespace App\Tests\Integration\Shared\Infrastructure\DomainEvents;

use App\Tests\Unit\Shared\Domain\Events\DomainEventForTest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Transport\TransportInterface;

class DomainEventsTest extends KernelTestCase
{
    public function testDomainEventsShouldBePublishedOnLocalAndEventStoreQueue()
    {
        self::bootKernel();

        /** @var TransportInterface $localTransport */
        $localTransport = self::$container->get('messenger.transport.sync');
        /** @var TransportInterface $eventStoreTransport */
        $eventStoreTransport = self::$container->get('messenger.transport.event_store');

        /** @var MessageBus $eventBus */
        $eventBus = self::$container->get('event_bus');
        $eventBus->dispatch(DomainEventForTest::build('only for tests purposes'));

        self::assertInstanceOf(DomainEventForTest::class, $localTransport->get()[0]->getMessage());
        self::assertInstanceOf(DomainEventForTest::class, $eventStoreTransport->get()[0]->getMessage());
    }
}
