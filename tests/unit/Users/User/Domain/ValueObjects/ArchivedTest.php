<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\ValueObjects;

use App\Users\User\Domain\ValueObjects\Archived;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class ArchivedTest extends TestCase
{
    public function testEmailCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(Archived::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testArchivedCanBeBuilt(): void
    {
        $archived = Archived::build(true);

        self::assertInstanceOf(Archived::class, $archived);
        self::assertTrue($archived->value());
    }
}
