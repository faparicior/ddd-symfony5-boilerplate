<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\ValueObjects;

use App\Users\User\Domain\Exceptions\UserNameInvalidByPolicyRulesException;
use App\Users\User\Domain\ValueObjects\UserName;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class UserNameTest extends TestCase
{
    private const INVALID_BY_POLICY_RULES = 'Username invalid by policy rules';

    public function testUserNameCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(UserName::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testUserNameCanBeBuilt(): void
    {
        $userName = UserName::build('UserTest');

        self::assertInstanceOf(UserName::class, $userName);
        self::assertEquals('UserTest', $userName->value());
    }

    public function testUserNameCanBeEmptyValue(): void
    {
        self::expectException(UserNameInvalidByPolicyRulesException::class);
        self::expectExceptionMessage(self::INVALID_BY_POLICY_RULES);

        UserName::build('');
    }
}
