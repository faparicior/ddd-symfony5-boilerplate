<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\ValueObjects;

use App\Users\User\Domain\ValueObjects\Email;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class EmailTest extends TestCase
{
    public function testEmailCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(Email::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testEmailCanBeBuilt(): void
    {
        $email = Email::build('test@test.de');

        self::assertInstanceOf(Email::class, $email);
        self::assertEquals('test@test.de', $email->value());
    }
}
