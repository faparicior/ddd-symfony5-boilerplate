<?php declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\Events;

use App\Users\User\Domain\Events\UserWasSignedUp;
use App\Users\User\Domain\ValueObjects\UserId;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class UserWasSignedUpTest extends TestCase
{
    public function testCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(UserWasSignedUp::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testCanBeBuild(): void
    {
        $event = UserWasSignedUp::build(UserId::build());

        self::assertInstanceOf(UserWasSignedUp::class, $event);
    }

    public function testCanReturnDataAsString(): void
    {
        $userId = UserId::build();
        $expected = "UserId: " . $userId->value();
        $event = UserWasSignedUp::build($userId);

        self::assertEquals($expected, $event->__toString());
    }
}
