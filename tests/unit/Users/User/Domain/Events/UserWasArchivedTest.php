<?php declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\Events;

use App\Users\User\Domain\Events\UserWasArchived;
use App\Users\User\Domain\ValueObjects\UserId;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class UserWasArchivedTest extends TestCase
{
    public function testCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(UserWasArchived::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testCanBeBuild(): void
    {
        $event = UserWasArchived::build(UserId::build());

        self::assertInstanceOf(UserWasArchived::class, $event);
    }
    
    public function testCanReturnDataAsString(): void
    {
        $userId = UserId::build();
        $expected = "UserId: " . $userId->value();
        $event = UserWasArchived::build($userId);

        self::assertEquals($expected, $event->__toString());
    }
}
