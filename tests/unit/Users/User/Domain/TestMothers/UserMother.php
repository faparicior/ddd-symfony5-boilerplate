<?php declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\TestMothers;

use App\Shared\Domain\Exceptions\DomainException;
use App\Shared\Domain\Exceptions\InvalidEmailException;
use App\Users\User\Domain\Exceptions\PasswordInvalidByPolicyRulesException;
use App\Users\User\Domain\Exceptions\UserNameInvalidByPolicyRulesException;
use App\Users\User\Domain\User;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use ReflectionException;

class UserMother
{
    public const USER_UUID = '73f2791e-eaa7-4f81-a8cc-7cc601cda30e';
    public const USERNAME = 'Test User';
    public const EMAIL = 'test@test.de';
    public const PASSWORD = 'userpass';

    /**
     * @return User
     * @throws DomainException
     * @throws InvalidEmailException
     * @throws PasswordInvalidByPolicyRulesException
     * @throws UserNameInvalidByPolicyRulesException
     * @throws ReflectionException
     */
    public static function forTest(): User
    {
        return User::signUp(
            UserId::fromString(self::USER_UUID),
            UserName::build(self::USERNAME),
            Email::build(self::EMAIL),
            Password::build(self::PASSWORD)
        );
    }
}
