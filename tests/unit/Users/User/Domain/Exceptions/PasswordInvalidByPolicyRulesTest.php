<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\Exceptions;

use App\Users\User\Domain\Exceptions\PasswordInvalidByPolicyRulesException;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class PasswordInvalidByPolicyRulesTest extends TestCase
{
    private const TEST_MESSAGE = 'TestMessage';
    private const TEST_CODE = 2;
    private const INVALID_PASSWORD_DEFAULT_MESSAGE = 'Password invalid by policy rules';

    public function testPasswordInvalidByPolicyRulesExceptionCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(PasswordInvalidByPolicyRulesException::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testPasswordInvalidByPolicyRulesExceptionCanBeCreatedWithDefaultMessage(): void
    {
        $exception = PasswordInvalidByPolicyRulesException::build();

        self::assertEquals($exception->getMessage(), self::INVALID_PASSWORD_DEFAULT_MESSAGE);
    }

    public function testPasswordInvalidByPolicyRulesExceptionCanBeCreatedWithMessageAndStatusCode(): void
    {
        $exception = PasswordInvalidByPolicyRulesException::build(self::TEST_MESSAGE, self::TEST_CODE);

        self::assertEquals(self::TEST_MESSAGE, $exception->getMessage());
        self::assertEquals(self::TEST_CODE, $exception->getCode());
    }
}
