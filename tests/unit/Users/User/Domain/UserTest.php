<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain;

use App\Tests\Unit\Users\User\Domain\TestMothers\UserMother;
use App\Users\User\Domain\Events\UserWasArchived;
use App\Users\User\Domain\Events\UserWasSignedUp;
use App\Users\User\Domain\User;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class UserTest extends TestCase
{
    public function testUserCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(User::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testUserCanBeBuilt(): void
    {
        $user = UserMother::forTest();

        self::assertInstanceOf(User::class, $user);
        self::assertEquals(UserMother::USER_UUID, $user->userId()->value());
        self::assertEquals(UserMother::USERNAME, $user->username()->value());
        self::assertEquals(UserMother::EMAIL, $user->email()->value());
        self::assertEquals(UserMother::PASSWORD, $user->password()->value());
    }

    public function testUserCanBeArchived(): void
    {
        $user = UserMother::forTest();
        $user->archive();
        self::assertTrue($user->archived()->value());
    }

    public function testCanCollectEventOnCreation(): void
    {
        $user = UserMother::forTest();
        $events = $user->getEvents();

        self::assertInstanceOf(UserWasSignedUp::class, $events[0]);
    }

    public function testCanCollectEventOnArchive(): void
    {
        $user = UserMother::forTest();
        $user->archive();
        $events = $user->getEvents();

        self::assertInstanceOf(UserWasArchived::class, $events[1]);
    }
}
