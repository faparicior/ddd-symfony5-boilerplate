<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\Specifications;

use App\Users\User\Domain\Specifications\StringMoreThanSevenCharacters;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class StringMoreThanSevenCharactersTest extends TestCase
{
    private const VALID_STRING = '12345678';
    private const INVALID_STRING = '1234567';
    private const SPECIFICATION_FAIL_MESSAGE = 'String less than 8 characters';

    public function testMoreThanSevenCharactersCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(StringMoreThanSevenCharacters::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testMoreThanSevenCharactersValidationReturnsTrue(): void
    {
        $specification = StringMoreThanSevenCharacters::build();

        self::assertTrue($specification->isSatisfiedBy(self::VALID_STRING));
    }

    public function testMoreThanSevenCharactersValidationReturnsFalse(): void
    {
        $specification = StringMoreThanSevenCharacters::build();

        self::assertFalse($specification->isSatisfiedBy(self::INVALID_STRING));
    }

    public function testMoreThanSevenCharactersReturnsExpectedFailedMessage(): void
    {
        $specification = StringMoreThanSevenCharacters::build();
        self::assertEquals(self::SPECIFICATION_FAIL_MESSAGE, $specification->getFailedMessage());
    }
}
