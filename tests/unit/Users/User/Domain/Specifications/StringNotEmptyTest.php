<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Domain\Specifications;

use App\Users\User\Domain\Specifications\StringNotEmpty;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class StringNotEmptyTest extends TestCase
{
    private const VALID_STRING = '12345678';
    private const INVALID_STRING = '';
    private const SPECIFICATION_FAIL_MESSAGE = 'String value empty';

    public function testStringNotEmptyCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(StringNotEmpty::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testStringNotEmptyValidationReturnsTrue(): void
    {
        $specification = StringNotEmpty::build();

        self::assertTrue($specification->isSatisfiedBy(self::VALID_STRING));
    }

    public function testStringNotEmptyValidationReturnsFalse(): void
    {
        $specification = StringNotEmpty::build();

        self::assertFalse($specification->isSatisfiedBy(self::INVALID_STRING));
    }

    public function testUsernameExistsReturnsExpectedFailedMessage(): void
    {
        $specification = StringNotEmpty::build();
        self::assertEquals(self::SPECIFICATION_FAIL_MESSAGE, $specification->getFailedMessage());
    }
}
