<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\SignUpUser;

use App\Shared\Infrastructure\Services\UniqueIdProviderInterface;
use App\Shared\Infrastructure\Services\UniqueIdProviderStub;
use App\Tests\Unit\Shared\Infrastructure\MessageBus\MessageBusDummy;
use App\Users\User\Application\Services\UserBuilder;
use App\Users\User\Application\SignUpUser\SignUpUserCommand;
use App\Users\User\Application\SignUpUser\SignUpUserCommandHandler;
use App\Users\User\Application\Specifications\SignUpUserSpecificationChain;
use App\Users\User\Application\Specifications\UserSpecificationInterface;
use App\Users\User\Domain\User;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Infrastructure\Persistence\InMemoryUserRepository;
use App\Users\User\Ui\Http\Api\Rest\SignUpUserPresenter;
use Exception;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidFactory;

class UserSpecificationStub implements UserSpecificationInterface
{
    public function isSatisfiedBy(User $user): bool
    {
        return true;
    }

    public function getFailedMessage(): string
    {
        // TODO: Implement getFailedMessage() method.
    }
}

class SignUpUserCommandHandlerTest extends TestCase
{
    private const USER_UUID = UniqueIdProviderStub::USER_UUID;
    private const USERNAME = 'JohnDoe';
    private const EMAIL = 'test.email@gmail.com';
    private const PASSWORD = ",&+3RjwAu88(tyC'";

    private UniqueIdProviderInterface $uuidService;
    private UserRepositoryInterface $userRepository;
    private UserBuilder $userBuilder;
    private SignUpUserPresenter $signUpUserPresenter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->uuidService = new UniqueIdProviderStub(new UuidFactory());
        $this->uuidService->setUuidToReturn(self::USER_UUID);
        $this->signUpUserPresenter = new SignUpUserPresenter();

        $this->userRepository = new InMemoryUserRepository();
        $this->userBuilder = new UserBuilder(
            $this->userRepository,
            SignUpUserSpecificationChain::build(...[new UserSpecificationStub()]),
            new MessageBusDummy()
        );
    }

    /**
     * @throws Exception
     */
    public function testSignUpUserCommandHandlerReturnsAValidPresenter(): void
    {
        $command = SignUpUserCommand::build(
            self::USERNAME,
            self::EMAIL,
            self::PASSWORD
        );

        $response = $this->handleCommand($command);
        self::assertInstanceOf(SignUpUserPresenter::class, $response);
    }

    /**
     * @throws Exception
     */
    public function testSignUpUserCommandHandlerStoreAnUser(): void
    {
        $command = SignUpUserCommand::build(
            self::USERNAME,
            self::EMAIL,
            self::PASSWORD
        );

        $response = $this->handleCommand($command);
        $userResponse = $response->read();

        $user = $this->userRepository->findById(UserId::fromString($userResponse['id']));

        self::assertEquals(self::USER_UUID, $user->userId()->value());
    }

    /**
     * @throws Exception
     */
    private function handleCommand(SignUpUserCommand $command): SignUpUserPresenter
    {
        $commandHandler = new SignUpUserCommandHandler(
            $this->uuidService,
            $this->userBuilder,
            $this->signUpUserPresenter
        );

        return $commandHandler($command);
    }
}
