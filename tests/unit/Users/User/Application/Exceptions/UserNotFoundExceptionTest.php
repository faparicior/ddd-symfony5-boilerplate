<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\Exceptions;

use App\Users\User\Application\Exceptions\UserNotFoundException;
use Error;
use PHPUnit\Framework\TestCase;

class UserNotFoundExceptionTest extends TestCase
{
    private const USER_NOT_FOUND_MESSAGE = 'User not found';

    public function testUserNotFoundExceptionCannotBeInstantiated(): void
    {
        self::expectException(Error::class);

        new UserNotFoundException();
    }

    public function testUserNotFoundExceptionCanBeBuilt(): void
    {
        $applicationException = UserNotFoundException::build();
        self::assertEquals(self::USER_NOT_FOUND_MESSAGE, $applicationException->getMessage());
    }
}
