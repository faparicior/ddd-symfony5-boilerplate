<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\DeleteUser;

use App\Shared\Domain\ValueObjects\BooleanValue;
use App\Tests\Unit\Shared\Infrastructure\MessageBus\MessageBusDummy;
use App\Users\User\Application\DeleteUser\DeleteUserCommand;
use App\Users\User\Application\DeleteUser\DeleteUserCommandHandler;
use App\Users\User\Application\Exceptions\UserNotFoundException;
use App\Users\User\Domain\User;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use App\Users\User\Infrastructure\Persistence\InMemoryUserRepository;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

class DeleteUserCommandHandlerTest extends TestCase
{
    private const USERNAME = 'JohnDoe';
    private const EMAIL = 'test.email@gmail.com';
    private const NON_EXISTENT_USER_ID = 'e48930cb-4890-4175-a9b7-3ae3d6c9c0e6';
    private const PASSWORD = ",&+3RjwAu88(tyC'";
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $eventBus;
    private UserId $userId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userId = UserId::build();

        $this->eventBus = new MessageBusDummy();

        $this->userRepository = new InMemoryUserRepository();

        $this->userRepository->store(User::signUp(
            $this->userId,
            UserName::build(self::USERNAME),
            Email::build(self::EMAIL),
            Password::build(self::PASSWORD)
        ));
    }

    /**
     * @throws Exception
     */
    public function testDeleteUserCommandHandlerReturnsAValidResponse(): void
    {
        $command = DeleteUserCommand::build($this->userId->value());

        $response = $this->handleCommand($command);

        self::assertTrue($response->value());
    }

    /**
     * @throws Exception
     */
    public function testDeleteUserCommandDeleteTheUser(): void
    {
        $command = DeleteUserCommand::build($this->userId->value());

        $response = $this->handleCommand($command);

        $user = $this->userRepository->findById($this->userId);

        self::assertTrue($response->value());
        self::assertNull($user);
    }

    public function testUserCannotBeFoundThrowsAnError(): void
    {
        self::expectException(UserNotFoundException::class);

        $command = DeleteUserCommand::build(self::NON_EXISTENT_USER_ID);
        $this->handleCommand($command);
    }

    /**
     * @param $command
     *
     * @return BooleanValue
     *
     * @throws Exception|UserNotFoundException
     */
    private function handleCommand(DeleteUserCommand $command): BooleanValue
    {
        $commandHandler = new DeleteUserCommandHandler($this->userRepository, $this->eventBus);

        return $commandHandler($command);
    }
}
