<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\DeleteUser;

use App\Users\User\Application\DeleteUser\DeleteUserCommand;
use Error;
use PHPUnit\Framework\TestCase;

class DeleteUserCommandTest extends TestCase
{
    private const USER_ID = 'a281ffdb-4fa3-4430-95cc-b072e16701a2';

    public function testDeleteUserCommandCannotBeInstantiatedDirectly(): void
    {
        self::expectException(Error::class);

        new DeleteUserCommand(
            self::USER_ID
        );
    }

    public function testDeleteUserCommandCanBeBuilt(): void
    {
        $deleteUserCommand = DeleteUserCommand::build(
            self::USER_ID
        );

        self::assertInstanceOf(DeleteUserCommand::class, $deleteUserCommand);
        self::assertEquals(self::USER_ID, $deleteUserCommand->userId());
    }
}
