<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\ArchiveUser;

use App\Shared\Domain\ValueObjects\BooleanValue;
use App\Tests\Unit\Shared\Infrastructure\MessageBus\MessageBusDummy;
use App\Users\User\Application\ArchiveUser\ArchiveUserCommand;
use App\Users\User\Application\ArchiveUser\ArchiveUserCommandHandler;
use App\Users\User\Application\Exceptions\UserNotFoundException;
use App\Users\User\Domain\User;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use App\Users\User\Infrastructure\Persistence\InMemoryUserRepository;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

class ArchiveUserCommandHandlerTest extends TestCase
{
    private const USERNAME = 'JohnDoe';
    private const EMAIL = 'test.email@gmail.com';
    private const NON_EXISTENT_EMAIL = 'no-email@gmail.com';
    private const PASSWORD = ",&+3RjwAu88(tyC'";
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $eventBus;

    protected function setUp(): void
    {
        parent::setUp();

        $this->eventBus = new MessageBusDummy();

        $this->userRepository = new InMemoryUserRepository();

        $this->userRepository->store(User::signUp(
            UserId::build(),
            UserName::build(self::USERNAME),
            Email::build(self::EMAIL),
            Password::build(self::PASSWORD)
        ));
    }

    /**
     * @throws Exception
     */
    public function testArchiveUserCommandHandlerReturnsAValidResponse(): void
    {
        $command = ArchiveUserCommand::build(self::EMAIL);

        $response = $this->handleCommand($command);

        self::assertTrue($response->value());
    }

    /**
     * @throws Exception
     */
    public function testArchiveUserCommandHandlerMarkTheUserArchived(): void
    {
        $command = ArchiveUserCommand::build(self::EMAIL);

        $response = $this->handleCommand($command);

        $user = $this->userRepository->findByEmail(Email::build(self::EMAIL));

        self::assertTrue($response->value());
        self::assertTrue($user->archived()->value());
    }

    public function testUserCannotBeFoundThrowsAnError(): void
    {
        self::expectException(UserNotFoundException::class);

        $command = ArchiveUserCommand::build(self::NON_EXISTENT_EMAIL);
        $this->handleCommand($command);
    }

    /**
     * @param $command
     *
     * @return BooleanValue
     *
     * @throws Exception|UserNotFoundException
     */
    private function handleCommand(ArchiveUserCommand $command): BooleanValue
    {
        $commandHandler = new ArchiveUserCommandHandler($this->userRepository, $this->eventBus);

        return $commandHandler($command);
    }
}
