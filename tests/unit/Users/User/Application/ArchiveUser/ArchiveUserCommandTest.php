<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Application\ArchiveUser;

use App\Users\User\Application\ArchiveUser\ArchiveUserCommand;
use Error;
use PHPUnit\Framework\TestCase;

class ArchiveUserCommandTest extends TestCase
{
    private const EMAIL = 'test@test.de';

    public function testDeleteUserCommandCannotBeInstantiatedDirectly(): void
    {
        self::expectException(Error::class);

        new ArchiveUserCommand(
            self::EMAIL
        );
    }

    public function testDeleteUserCommandCanBeBuilt(): void
    {
        $deleteUserCommand = ArchiveUserCommand::build(
            self::EMAIL
        );

        self::assertInstanceOf(ArchiveUserCommand::class, $deleteUserCommand);
        self::assertEquals(self::EMAIL, $deleteUserCommand->email());
    }
}
