<?php declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Ui\Http\Api\Rest;

use App\Tests\Unit\Users\User\Domain\TestMothers\UserMother;
use App\Users\User\Ui\Http\Api\Rest\SignUpUserPresenter;
use PHPUnit\Framework\TestCase;

class SignUpUserPresenterTest extends TestCase
{
    public function testSignUpCanReturnContentsAsArray(): void
    {
        $signUpUserPresenter = new SignUpUserPresenter();
        $signUpUserPresenter->write(UserMother::forTest());
        $result = $signUpUserPresenter->read();

        $expected = [
            'id' => UserMother::USER_UUID,
            'userName' => UserMother::USERNAME,
            'email' => UserMother::EMAIL,
            'password' => UserMother::PASSWORD
        ];

        self::assertEquals($expected, $result);
    }
}
