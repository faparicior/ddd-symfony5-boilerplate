<?php

declare(strict_types=1);

namespace App\Tests\Unit\Users\User\Ui\Http\Api\Rest;

use App\Tests\Unit\Shared\Infrastructure\MessageBus\MessageBusDummy;
use App\Tests\Unit\Shared\Infrastructure\MessageBus\MessageBusStub;
use App\Users\User\Application\ArchiveUser\ArchiveUserCommandHandler;
use App\Users\User\Domain\User;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use App\Users\User\Infrastructure\Persistence\InMemoryUserRepository;
use App\Users\User\Ui\Http\Api\Rest\ArchiveUser;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArchiveUserTest extends TestCase
{
    private const USERNAME = 'JohnDoe';
    private const EMAIL = 'test.email@gmail.com';
    private const NON_EXISTENT_EMAIL = 'non-email@gmail.com';
    private const PASSWORD = ",&+3RjwAu88(tyC'";

    private ArchiveUser $controller;
    private UserRepositoryInterface $userRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userRepository = new InMemoryUserRepository();
        $eventBus = new MessageBusDummy();

        $this->userRepository->store(User::signUp(
            UserId::build(),
            UserName::build(self::USERNAME),
            Email::build(self::EMAIL),
            Password::build(self::PASSWORD)
        ));

        $deleteUserCommandHandler = new ArchiveUserCommandHandler($this->userRepository, $eventBus);

        $commandBus = new MessageBusStub($deleteUserCommandHandler);
        $queryBus = new MessageBusStub();
        $log = new Logger('genericLog');
        $domainLog = new Logger('domainLog');

        $this->controller = new ArchiveUser($commandBus, $queryBus, $log, $domainLog);
    }

    public function testUserCanBeDeleted(): void
    {
        $data = json_encode([
            'email' => self::EMAIL,
        ]);

        $request = Request::create('/users', 'DELETE', [], [], [], [], $data);

        $response = $this->controller->execute($request);

        self::assertJsonStringEqualsJsonString('{}', $response->getContent());
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testDeleteUserReturnsBadRequestWithNonExistentUser(): void
    {
        $data = json_encode([
            'email' => self::NON_EXISTENT_EMAIL,
        ]);

        $request = Request::create('/users', 'DELETE', [], [], [], [], $data);

        $response = $this->controller->execute($request);

        self::assertEquals('"User not found"', $response->getContent());
        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }
}
