<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Infrastructure\MessageBus;

use stdClass;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class MessageBusDummy implements MessageBusInterface
{
    public function dispatch($message, array $stamps = []): Envelope
    {
        return new Envelope(new stdClass());
    }
}
