<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Infrastructure\MessageBus;

use stdClass;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class MessageBusSpy implements MessageBusInterface
{
    private $handler;
    private array $eventsDispatched;

    public function __construct($handler = null)
    {
        $this->handler = $handler;
        $this->eventsDispatched = [];
    }

    public function dispatch($message, array $stamps = []): Envelope
    {
//        $result = $this->handler->__invoke($message);
        $result = new stdClass();
        $handlerName = 'StubbedHandler';
        $this->eventsDispatched[] = $message;

        $handledStamp = new HandledStamp($result, $handlerName);
        return Envelope::wrap($result, [$handledStamp]);
    }

    public function eventsDispatched(): int
    {
        return count($this->eventsDispatched);
    }

    public function eventDispatchedAtOrder(int $order): ?object
    {
        return $this->eventsDispatched[$order - 1];
    }
}
