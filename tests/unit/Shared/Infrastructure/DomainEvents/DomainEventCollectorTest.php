<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Infrastructure\DomainEvents;

use App\Shared\Infrastructure\DomainEvents\DomainEventCollectorTrait;
use App\Tests\Unit\Shared\Domain\Events\DomainEventForTest;
use PHPUnit\Framework\TestCase;

class EntityWithDomainEventCollector
{
    use DomainEventCollectorTrait;
}

class DomainEventCollectorTest extends TestCase
{
    private const SOME_VALUE = 'some_value';
    private EntityWithDomainEventCollector $entity;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entity = new EntityWithDomainEventCollector();
        $this->entity->collectEvent(DomainEventForTest::build(self::SOME_VALUE));
    }

    public function testCanCollectEvents(): void
    {
        $events = $this->entity->getEvents();

        self::assertCount(1, $events);
        self::assertInstanceOf(DomainEventForTest::class, $events[0]);
    }

    public function testWhenReturnEventsAlsoFlushesThem(): void
    {
        $events = $this->entity->getEvents();
        self::assertCount(1, $events);

        $events = $this->entity->getEvents();
        self::assertCount(0, $events);
    }

    public function testCanFlushEvents(): void
    {
        $this->entity->flushEvents();
        $events = $this->entity->getEvents();

        self::assertCount(0, $events);
    }
}
