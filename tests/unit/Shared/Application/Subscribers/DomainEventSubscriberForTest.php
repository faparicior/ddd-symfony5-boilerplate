<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Application\Subscribers;

use App\Shared\Application\Subscribers\DomainEventSubscriber;

class DomainEventSubscriberForTest extends DomainEventSubscriber
{
    protected static function eventsSubscribed(): array
    {
        return [
            'some_event'
        ];
    }
}
