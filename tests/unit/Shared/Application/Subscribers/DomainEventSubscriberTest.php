<?php

namespace App\Tests\Unit\Shared\Application\Subscribers;

use App\Shared\Application\Subscribers\DomainEventSubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

class DomainEventSubscriberTest extends TestCase
{
    public function testSubscriberCanReturnEventsSubscribed()
    {
        $subscriber = new DomainEventSubscriberForTest();
        
        self::assertIsIterable($subscriber::getHandledMessages());
        self::assertEquals(
            [
                'some_event' => [
                    'bus' => 'event_bus'
                ]
            ],
            iterator_to_array($subscriber::getHandledMessages())
        );
    }

    public function testExtendsFromSymfonyMessenger()
    {
        $subscriber = new DomainEventSubscriberForTest();
        $subscriberClass = new \ReflectionClass($subscriber);

        self::assertInstanceOf(DomainEventSubscriber::class, $subscriber);
        self::assertTrue($subscriberClass->implementsInterface(MessageSubscriberInterface::class));
    }
}
