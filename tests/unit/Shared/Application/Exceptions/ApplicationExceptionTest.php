<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Application\Exceptions;

use App\Shared\Application\Exceptions\ApplicationException;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class DummyApplicationException extends ApplicationException
{
}

class ApplicationExceptionTest extends TestCase
{
    public function testApplicationExceptionCannotBeInstantiated(): void
    {
        $reflection = new ReflectionMethod(ApplicationException::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testApplicationExceptionCanBeBuilt(): void
    {
        $applicationException = DummyApplicationException::build('message', 100);
        self::assertEquals('message', $applicationException->getMessage());
        self::assertEquals(100, $applicationException->getCode());
    }
}
