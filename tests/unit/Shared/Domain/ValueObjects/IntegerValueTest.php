<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\ValueObjects;

use App\Shared\Domain\ValueObjects\IntegerValue;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class IntegerForTest extends IntegerValue
{
}

class IntegerValueTest extends TestCase
{
    public function testIntegerValueCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(IntegerValue::class, '__construct');
        self::assertFalse($reflection->isPublic());

    }

    public function testIntegerValueIsAccessible(): void
    {
        $integer = IntegerForTest::build(12);

        self::assertEquals(12, $integer->value());
    }

    public function testEqualsFunction(): void
    {
        $integer = IntegerForTest::build(14);

        self::assertTrue($integer->equals(IntegerForTest::build(14)));
        self::assertFalse($integer->equals(IntegerForTest::build(15)));
    }
}
