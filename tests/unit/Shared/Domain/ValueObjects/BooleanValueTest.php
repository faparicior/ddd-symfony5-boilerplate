<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\ValueObjects;

use App\Shared\Domain\ValueObjects\BooleanValue;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class BooleanForTest extends BooleanValue
{
}

class BooleanValueTest extends TestCase
{
    public function testBooleanValueCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(BooleanValue::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testBooleanValueIsAccessible(): void
    {
        $boolean = BooleanForTest::build(true);

        self::assertEquals(true, $boolean->value());
    }

    public function testEqualsFunction(): void
    {
        $boolean = BooleanForTest::build(true);

        self::assertTrue($boolean->equals(BooleanForTest::build(true)));
        self::assertFalse($boolean->equals(BooleanForTest::build(false)));
    }

    public function testHasToReturnToStringValue(): void
    {
        $boolean = BooleanForTest::build(true);
        self::assertEquals('true', $boolean->__toString());

        $boolean = BooleanForTest::build(false);
        self::assertEquals('false', $boolean->__toString());
    }

    public function testHasToBeBuildFromString(): void
    {
        $boolean = BooleanForTest::fromString('true');
        self::assertTrue($boolean->value());
        $boolean = BooleanForTest::fromString('false');
        self::assertFalse($boolean->value());
        $boolean = BooleanForTest::fromString('OTHER_VALUES');
        self::assertFalse($boolean->value());
    }
}
