<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exceptions;

use App\Shared\Domain\Exceptions\DomainException;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class DomainExceptionForTest extends DomainException
{
}

class DomainExceptionTest extends TestCase
{
    private const TEST_MESSAGE = 'TestMessage';
    private const TEST_CODE = 2;

    public function testExceptionCannotBeInstantiatedDirectly(): void
    {
        $reflection = new ReflectionMethod(DomainException::class, '__construct');
        self::assertFalse($reflection->isPublic());
    }

    public function testExceptionCanBeBuilt(): void
    {
        $exception = DomainExceptionForTest::build();

        self::assertInstanceOf(DomainExceptionForTest::class, $exception);
    }

    public function testExceptionCanBeCreatedWithMessageAndStatusCode(): void
    {
        $exception = DomainExceptionForTest::build(self::TEST_MESSAGE, self::TEST_CODE);

        self::assertEquals(self::TEST_MESSAGE, $exception->getMessage());
        self::assertEquals(self::TEST_CODE, $exception->getCode());
    }
}
