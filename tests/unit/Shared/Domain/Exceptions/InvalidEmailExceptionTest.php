<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exceptions;

use App\Shared\Domain\Exceptions\InvalidEmailException;
use PHPUnit\Framework\TestCase;

class InvalidEmailExceptionTest extends TestCase
{
    private const TEST_MESSAGE = 'TestMessage';
    private const TEST_CODE = 2;
    private const INVALID_EMAIL_DEFAULT_MESSAGE = 'Invalid Email format';

    public function testInvalidEmailExceptionCannotBeInstantiatedDirectly(): void
    {
        self::expectException(\Error::class);

        new InvalidEmailException();
    }

    public function testInvalidEmailExceptionCanBeCreatedWithDefaultMessage(): void
    {
        $exception = InvalidEmailException::build();

        self::assertEquals(self::INVALID_EMAIL_DEFAULT_MESSAGE, $exception->getMessage());
    }

    public function testInvalidEmailExceptionCanBeCreatedWithMessageAndStatusCode(): void
    {
        $exception = InvalidEmailException::build(self::TEST_MESSAGE, self::TEST_CODE);

        self::assertEquals(self::TEST_MESSAGE, $exception->getMessage());
        self::assertEquals(self::TEST_CODE, $exception->getCode());
    }
}
