<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Specifications;

use App\Shared\Domain\Specifications\SpecificationChain;
use App\Shared\Domain\Specifications\SpecificationInterface;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class SpecificationChainStub extends SpecificationChain
{
    /**
     * @throws ReflectionException
     */
    public function __construct()
    {
        parent::__construct();

        $specificationFail = new SpecificationFailStub();
        $specificationFail2 = new SpecificationFail2Stub();

        $this->processSpecificationResult($specificationFail->isSatisfiedBy(), $specificationFail);
        $this->processSpecificationResult($specificationFail2->isSatisfiedBy(), $specificationFail2);
    }
}


class SpecificationFailStub implements SpecificationInterface
{
    public function isSatisfiedBy(): bool
    {
        return false;
    }

    public function getFailedMessage(): string
    {
        return 'Data invalid using specifications';
    }
}

class SpecificationFail2Stub implements SpecificationInterface
{
    public function isSatisfiedBy(): bool
    {
        return false;
    }

    public function getFailedMessage(): string
    {
        return 'Data2 invalid using specifications';
    }
}

class SpecificationChainTest extends TestCase
{
    public function testShouldReturnAStringWithFailedSpecifications(): void
    {
        $expected = 'Data invalid using specifications, Data2 invalid using specifications';
        $specification = new SpecificationChainStub();

        self::assertEquals($expected, $specification->getFailedResultsAsString());
    }
}
