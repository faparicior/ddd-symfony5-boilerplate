<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Specifications;

use App\Shared\Domain\Specifications\SpecificationChainResult;
use App\Shared\Domain\Specifications\SpecificationChainResultCollection;
use Error;
use PHPUnit\Framework\TestCase;

class SpecificationChainResultCollectionTest extends TestCase
{
    private SpecificationChainResultCollection $specificationChainResultCollection;
    private SpecificationChainResult $okSpecificationChainResult;
    private SpecificationChainResult $koSpecificationChainResult;
    private SpecificationChainResult $ko2SpecificationChainResult;

    protected function setUp(): void
    {
        parent::setUp();

        $this->specificationChainResultCollection = SpecificationChainResultCollection::build();
        $this->okSpecificationChainResult = SpecificationChainResult::build('spec ok', true, 'message ok');
        $this->koSpecificationChainResult = SpecificationChainResult::build('spec ko', false, 'message ko');
        $this->ko2SpecificationChainResult = SpecificationChainResult::build('spec ko 2', false, 'message ko2');
    }

    public function testCannotBeInstantiatedDirectly(): void
    {
        self::expectException(Error::class);

        new SpecificationChainResultCollection();
    }

    public function testCanAddAResult(): void
    {
        $this->specificationChainResultCollection->addResult($this->okSpecificationChainResult);

        self::assertEquals(1, $this->specificationChainResultCollection->count());
        self::assertEquals($this->okSpecificationChainResult, $this->specificationChainResultCollection->first());
    }

    public function testCanGetResultsAsArrayOfSpecificationChainResult(): void
    {
        $this->specificationChainResultCollection->addResult($this->okSpecificationChainResult);

        $arrayResults = $this->specificationChainResultCollection->getResults();

        self::assertIsArray($arrayResults);
        self::assertInstanceOf(SpecificationChainResult::class, $arrayResults[0]);
    }

    public function testCanGetErrorResultsAsArrayOfSpecificationChainResult(): void
    {
        $this->specificationChainResultCollection->addResult($this->okSpecificationChainResult);
        $this->specificationChainResultCollection->addResult($this->koSpecificationChainResult);

        $failedResults = $this->specificationChainResultCollection->getFailedResults();

        self::assertCount(1, $failedResults);
        self::assertEquals($this->koSpecificationChainResult, $failedResults[0]);
    }

    public function testShouldReturnErrorResultsAsAConcatenatedString(): void
    {
        $expected = 'message ko, message ko2';

        $this->specificationChainResultCollection->addResult($this->okSpecificationChainResult);
        $this->specificationChainResultCollection->addResult($this->koSpecificationChainResult);
        $this->specificationChainResultCollection->addResult($this->ko2SpecificationChainResult);

        $failedResultsAsString = $this->specificationChainResultCollection->getFailedResultsAsString();

        self::assertEquals($expected, $failedResultsAsString);
    }
}
