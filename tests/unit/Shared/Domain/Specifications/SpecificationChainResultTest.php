<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Specifications;

use App\Shared\Domain\Specifications\SpecificationChainResult;
use Error;
use PHPUnit\Framework\TestCase;

class SpecificationChainResultTest extends TestCase
{
    public function testCannotBeInstantiatedDirectly(): void
    {
        self::expectException(Error::class);

        new SpecificationChainResult();
    }

    public function testCanBeBuilt(): void
    {
        $specificationChainResult = SpecificationChainResult::build('specification', true, 'message');
        self::assertEquals('specification', $specificationChainResult->specification());
        self::assertEquals(true, $specificationChainResult->value());
        self::assertEquals('message', $specificationChainResult->message());
    }
}
