<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Specifications;

use App\Shared\Domain\Specifications\SpecificationChainResult;
use App\Shared\Domain\Specifications\StringSpecificationChain;
use App\Shared\Domain\Specifications\StringSpecificationInterface;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class StringDummySpecification implements StringSpecificationInterface
{
    public function isSatisfiedBy(string $data): bool
    {
    }

    public function getFailedMessage(): string
    {
    }
}

class StringSpecificationOkStub implements StringSpecificationInterface
{
    public function isSatisfiedBy(string $data): bool
    {
        return true;
    }

    public function getFailedMessage(): string
    {
        return '';
    }
}

class StringSpecificationFailStub implements StringSpecificationInterface
{
    public function isSatisfiedBy(string $data): bool
    {
        return false;
    }

    public function getFailedMessage(): string
    {
        return 'String invalid using specifications';
    }
}

class StringSpecificationChainTest extends TestCase
{
    private const STRING_SPECIFICATION_OK_STUB = 'StringSpecificationOkStub';
    private const STRING_SPECIFICATION_FAIL_STUB = 'StringSpecificationFailStub';

    public function testStringSpecificationChainCannotBeInstantiatedDirectly(): void
    {
        self::expectException(\Error::class);

        new StringSpecificationChain();
    }

    public function testStringSpecificationChainCanBeCreated(): void
    {
        $specificationChain = StringSpecificationChain::build(...[(new StringDummySpecification())]);

        self::assertInstanceOf(StringSpecificationChain::class, $specificationChain);
    }

    /**
     * @throws ReflectionException
     */
    public function testStringSpecificationChainReturnFalseIfHasNoSpecifications(): void
    {
        $specificationChain = StringSpecificationChain::build();

        self::assertFalse($specificationChain->evalSpecifications(''));
    }

    /**
     * @throws ReflectionException
     */
    public function testStringSpecificationChainReturnTrueIfHasNoSpecifications(): void
    {
        $specificationChain = StringSpecificationChain::build(...[(new StringSpecificationOkStub())]);

        self::assertTrue($specificationChain->evalSpecifications(''));
    }

    /**
     * @throws ReflectionException
     */
    public function testStringSpecificationChainReturnSpecificationChainResults(): void
    {
        $specificationChain = StringSpecificationChain::build(...[(new StringSpecificationOkStub())]);

        self::assertTrue($specificationChain->evalSpecifications(''));
        /** @var SpecificationChainResult[] $results */
        $results = $specificationChain->getResultCollection()->getResults();

        self::assertEquals(self::STRING_SPECIFICATION_OK_STUB, $results[0]->specification());
        self::assertTrue($results[0]->value());
    }

    /**
     * @throws ReflectionException
     */
    public function testStringSpecificationChainReturnFalseWithOneFailedSpecification(): void
    {
        $specificationChain = StringSpecificationChain::build(...[(new StringSpecificationOkStub()), (new StringSpecificationFailStub())]);
        self::assertFalse($specificationChain->evalSpecifications(''));

        $specificationChain = StringSpecificationChain::build(...[(new StringSpecificationFailStub()), (new StringSpecificationOkStub())]);
        self::assertFalse($specificationChain->evalSpecifications(''));
    }

    /**
     * @throws ReflectionException
     */
    public function testStringSpecificationChainReturnSpecificationChainFailedResults(): void
    {
        $specificationChain = StringSpecificationChain::build(...[(new StringSpecificationOkStub()), (new StringSpecificationFailStub())]);

        self::assertFalse($specificationChain->evalSpecifications(''));
        $results = $specificationChain->getResultCollection()->getResults();

        self::assertEquals(self::STRING_SPECIFICATION_OK_STUB, $results[0]->specification());
        self::assertTrue($results[0]->value());
        self::assertEquals(self::STRING_SPECIFICATION_FAIL_STUB, $results[1]->specification());
        self::assertFalse($results[1]->value());

        $resultsFail = $specificationChain->getFailedResults();

        self::assertEquals('String invalid using specifications', $resultsFail[0]->message());
    }
}
