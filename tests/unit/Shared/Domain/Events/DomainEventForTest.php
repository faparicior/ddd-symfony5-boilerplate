<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Events;

use App\Shared\Domain\Events\DomainEvent;
use DateTimeImmutable;

class DomainEventForTest implements DomainEvent
{
    private string $attribute;
    
    private function __construct(string $value)
    {
        $this->attribute = $value;
    }

    /**
     * @return static
     */
    public static function build(string $value): self
    {
        return new static($value);
    }

    public function attribute(): string
    {
        return $this->attribute;
    }
    
    public function occurredOn(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

    public function __toString(): string
    {
        return 'attribute: ' . $this->attribute;
    }

    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }
}
