<?php declare(strict_types=1);

namespace App\Shared\Infrastructure\DomainEvents;

use App\Shared\Domain\Events\DomainEvent;
use Exception;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class ExternalJsonMessageSerializer implements SerializerInterface
{
    public function decode(array $encodedEnvelope): Envelope
    {
        // TODO: Implement decode() method.
    }

    public function encode(Envelope $envelope): array
    {
        $message = $envelope->getMessage();
        if ($message instanceof DomainEvent) {
            $data = [$message->toArray()];
        } else {
            throw new Exception('Unsupported message class');
        }

        $allStamps = [];
        foreach ($envelope->all() as $stamps) {
            $allStamps = array_merge($allStamps, $stamps);
        }

        return [
            'body' => json_encode($data),
//            'headers' => [
                // store stamps as a header - to be read in decode()
//                'stamps' => serialize($allStamps)
//            ],
        ];
    }
}
