<?php declare(strict_types=1);

namespace App\Shared\Infrastructure\DomainEvents;

use App\Shared\Domain\Events\DomainEvent;

trait DomainEventCollectorTrait
{
    /** @var DomainEvent[] */
    protected array $events = [];

    public function collectEvent(DomainEvent $domainEvent): void
    {
        $this->events[] = $domainEvent;
    }

    public function getEvents(): array
    {
        $events = $this->events;

        $this->flushEvents();

        return $events;
    }

    public function flushEvents(): void
    {
        $this->events = [];
    }
}
