<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Symfony\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201124182921 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX user_username_uindex');
        $this->addSql('DROP INDEX user_email_uindex');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT user_id, username, password, email FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (user_id  NOT NULL, username  NOT NULL, password  NOT NULL, email  NOT NULL, archived  DEFAULT \'false\' NOT NULL, PRIMARY KEY(user_id))');
        $this->addSql('INSERT INTO user (user_id, username, password, email) SELECT user_id, username, password, email FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT user_id, username, password, email FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (user_id CHAR(36) NOT NULL COLLATE BINARY, username VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(255) NOT NULL COLLATE BINARY, email VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(user_id))');
        $this->addSql('INSERT INTO user (user_id, username, password, email) SELECT user_id, username, password, email FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX user_username_uindex ON user (username)');
        $this->addSql('CREATE UNIQUE INDEX user_email_uindex ON user (email)');
    }
}
