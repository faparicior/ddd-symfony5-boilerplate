<?php declare(strict_types=1);

namespace App\Shared\Application\Subscribers;

use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

abstract class DomainEventSubscriber implements MessageSubscriberInterface
{
    private const EVENT_BUS = 'event_bus';

    abstract protected static function eventsSubscribed(): array;

    final public static function getHandledMessages(): iterable
    {
        yield from static::buildClassList();
    }

    final private static function buildClassList(): array
    {
        $classList = [];
        
        foreach (static::eventsSubscribed() as $class) {
            $classList[$class] = [
                //'method' => 'handleOtherSmsNotification',
                //'priority' => 0,
                //'from_transport' => 'sync',
                'bus' => self::EVENT_BUS
            ];
        }

        return $classList;
    }
}
