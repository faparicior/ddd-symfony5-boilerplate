<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObjects;

class BooleanValue
{
    private bool $value;

    private const TRUE_VALUES = [
        'true',
        '1'
    ];

    final private function __construct(bool $value)
    {
        $this->value = $value;
    }

    /**
     * @return static
     */
    final public static function build(bool $value): self
    {
        return new static($value);
    }

    public static function fromString(string $value)
    {
        return new static(in_array($value, self::TRUE_VALUES));
    }

    final public function value(): bool
    {
        return $this->value;
    }

    final public function equals(self $valueObject): bool
    {
        return $this->value === $valueObject->value();
    }

    final public function __toString(): string
    {
        return $this->value() ? 'true' : 'false';
    }
}
