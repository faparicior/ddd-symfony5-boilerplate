<?php

namespace App\Shared\Domain\Specifications;

interface SpecificationChainInterface
{
    public function getResultCollection(): SpecificationChainResultCollection;

    public function getFailedResults(): array;

    public function getFailedResultsAsString(): string;
}
