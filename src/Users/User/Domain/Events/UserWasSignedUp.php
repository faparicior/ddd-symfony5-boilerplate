<?php declare(strict_types=1);

namespace App\Users\User\Domain\Events;

use App\Shared\Domain\Events\DomainEvent;
use App\Users\User\Domain\ValueObjects\UserId;
use DateTimeImmutable;

class UserWasSignedUp implements DomainEvent
{
    private UserId $userId;
    private DateTimeImmutable $occurredOn;

    private function __construct(UserId $userId)
    {
        $this->occurredOn = new DateTimeImmutable();
        $this->userId = $userId;
    }
    
    public static function build(UserId $userId): UserWasSignedUp
    {
        return new UserWasSignedUp($userId);
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
    
    public function __toString(): string
    {
        return "UserId: " . $this->userId->value();
    }

    public function toArray(): array
    {
        return [
            "userId" =>  $this->userId->value()
        ];
    }
}
