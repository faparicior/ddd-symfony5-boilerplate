<?php

declare(strict_types=1);

namespace App\Users\User\Domain;

use App\Shared\Infrastructure\DomainEvents\DomainEventCollectorTrait;
use App\Users\User\Domain\Events\UserWasArchived;
use App\Users\User\Domain\Events\UserWasSignedUp;
use App\Users\User\Domain\ValueObjects\Archived;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;

class User
{
    use DomainEventCollectorTrait;

    private UserId $userId;
    private UserName $username;
    private Email $email;
    private Password $password;
    private Archived $archived;

    private function __construct(UserId $userId, UserName $userName, Email $email, Password $password, Archived $archived)
    {
        $this->userId = $userId;
        $this->username = $userName;
        $this->email = $email;
        $this->password = $password;
        $this->archived = $archived;
    }

    public static function signUp(UserId $userId, UserName $userName, Email $email, Password $password): self
    {
        $user = new static($userId, $userName, $email, $password, Archived::build(false));

        $user->collectEvent(UserWasSignedUp::build($userId));

        return $user;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function username(): UserName
    {
        return $this->username;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function password(): Password
    {
        return $this->password;
    }

    public function archived(): Archived
    {
        return $this->archived;
    }

    public function archive(): void
    {
        $this->archived = Archived::build(true);
        $this->collectEvent(UserWasArchived::build($this->userId));
    }
}
