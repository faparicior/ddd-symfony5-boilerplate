<?php

declare(strict_types=1);

namespace App\Users\User\Domain\ValueObjects;

use App\Shared\Domain\ValueObjects\BooleanValue;

class Archived extends BooleanValue
{
}
