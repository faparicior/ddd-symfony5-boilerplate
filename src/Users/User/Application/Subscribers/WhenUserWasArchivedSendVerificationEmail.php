<?php declare(strict_types=1);

namespace App\Users\User\Application\Subscribers;

use App\Shared\Application\Subscribers\SyncDomainEventSubscriber;
use App\Users\User\Domain\Events\UserWasArchived;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class WhenUserWasArchivedSendVerificationEmail extends SyncDomainEventSubscriber
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(UserWasArchived $userWasArchived)
    {
        $email = (new Email())
            ->from('test@test.de')
            ->to('test@test.com')
            ->subject('Do you want to delete your user?')
            ->text('Validate your mail bro...')
        ;
        $this->mailer->send($email);
    }

    protected static function eventsSubscribed(): array
    {
        return [
            UserWasArchived::class
        ];
    }
}
