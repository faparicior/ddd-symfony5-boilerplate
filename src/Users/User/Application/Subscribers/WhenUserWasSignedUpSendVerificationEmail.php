<?php declare(strict_types=1);

namespace App\Users\User\Application\Subscribers;

use App\Shared\Application\Subscribers\SyncDomainEventSubscriber;
use App\Users\User\Domain\Events\UserWasSignedUp;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class WhenUserWasSignedUpSendVerificationEmail extends SyncDomainEventSubscriber
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(UserWasSignedUp $userWasSignedUp)
    {
        $email = (new Email())
            ->from('test@test.de')
            ->to('test@test.com')
            ->subject('User was signedUp. Validate')
            ->text('Validate your mail bro...')
        ;
        $this->mailer->send($email);
    }

    protected static function eventsSubscribed(): array
    {
        return [
            UserWasSignedUp::class
        ];
    }
}
