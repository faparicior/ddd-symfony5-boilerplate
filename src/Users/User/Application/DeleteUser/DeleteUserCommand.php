<?php

declare(strict_types=1);

namespace App\Users\User\Application\DeleteUser;

final class DeleteUserCommand
{
    private string $userId;

    private function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    public static function build(string $email): self
    {
        return new static($email);
    }

    public function userId(): string
    {
        return $this->userId;
    }
}
