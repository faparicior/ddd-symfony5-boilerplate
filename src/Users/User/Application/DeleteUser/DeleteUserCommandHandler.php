<?php

declare(strict_types=1);

namespace App\Users\User\Application\DeleteUser;

use App\Shared\Application\CommandHandler;
use App\Shared\Domain\Exceptions\DomainException;
use App\Shared\Domain\ValueObjects\BooleanValue;
use App\Users\User\Application\Exceptions\UserNotFoundException;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\UserId;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

final class DeleteUserCommandHandler implements CommandHandler
{
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $eventBus;

    public function __construct(UserRepositoryInterface $userRepository, MessageBusInterface $eventBus)
    {
        $this->userRepository = $userRepository;
        $this->eventBus = $eventBus;
    }

    /**
     * @throws DomainException
     * @throws Exception|UserNotFoundException
     */
    public function __invoke(DeleteUserCommand $command): BooleanValue
    {
        $user = $this->userRepository->findById(UserId::fromString($command->userId()));

        if (is_null($user)) {
            throw UserNotFoundException::build();
        }

        $this->userRepository->delete($user);

        // TODO: Dispatch application event User purged

        return BooleanValue::build(true);
    }
}
