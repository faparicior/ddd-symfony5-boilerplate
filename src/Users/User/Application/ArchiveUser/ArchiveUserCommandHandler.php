<?php

declare(strict_types=1);

namespace App\Users\User\Application\ArchiveUser;

use App\Shared\Domain\Exceptions\DomainException;
use App\Shared\Domain\ValueObjects\BooleanValue;
use App\Shared\Application\CommandHandler;
use App\Users\User\Application\Exceptions\UserNotFoundException;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\Email;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

final class ArchiveUserCommandHandler implements CommandHandler
{
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $eventBus;

    public function __construct(UserRepositoryInterface $userRepository, MessageBusInterface $eventBus)
    {
        $this->userRepository = $userRepository;
        $this->eventBus = $eventBus;
    }

    /**
     * @throws DomainException
     * @throws Exception|UserNotFoundException
     */
    public function __invoke(ArchiveUserCommand $command): BooleanValue
    {
        $user = $this->userRepository->findByEmail(Email::build($command->email()));

        if (is_null($user)) {
            throw UserNotFoundException::build();
        }

        $user->archive();

        $this->userRepository->store($user);

        foreach ($user->getEvents() as $event) {
            $this->eventBus->dispatch($event);
        }

        return BooleanValue::build(true);
    }
}
