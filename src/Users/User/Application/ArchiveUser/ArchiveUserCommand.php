<?php

declare(strict_types=1);

namespace App\Users\User\Application\ArchiveUser;

final class ArchiveUserCommand
{
    private string $email;

    private function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return static
     */
    public static function build(string $email): self
    {
        return new static($email);
    }

    public function email(): string
    {
        return $this->email;
    }
}
