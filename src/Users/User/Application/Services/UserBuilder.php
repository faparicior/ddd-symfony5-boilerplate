<?php

declare(strict_types=1);

namespace App\Users\User\Application\Services;

use App\Users\User\Application\Exceptions\UserInvalidException;
use App\Users\User\Application\Specifications\SignUpUserSpecificationChain;
use App\Users\User\Domain\User;
use App\Users\User\Domain\UserRepositoryInterface;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use ReflectionException;
use Symfony\Component\Messenger\MessageBusInterface;

final class UserBuilder
{
    private UserRepositoryInterface $userRepository;
    private SignUpUserSpecificationChain $specificationChain;
    private MessageBusInterface $eventBus;

    public function __construct(
        UserRepositoryInterface $userRepository,
        SignUpUserSpecificationChain $userSpecifications,
        MessageBusInterface $eventBus
    ) {
        $this->userRepository = $userRepository;
        $this->specificationChain = $userSpecifications;
        $this->eventBus = $eventBus;
    }

    /**
     * @throws UserInvalidException
     * @throws ReflectionException
     */
    public function signUp(UserId $userId, UserName $userName, Email $email, Password $password): User
    {
        $user = User::signUp($userId, $userName, $email, $password);
        $this->guard($user);

        $this->userRepository->store($user);

        foreach ($user->getEvents() as $event) {
            $this->eventBus->dispatch($event);
        }

        return $user;
    }

    /**
     * @throws UserInvalidException
     * @throws ReflectionException
     */
    private function guard(User $user): void
    {
        if (isset($this->specificationChain)) {
            $isValid = $this->specificationChain->evalSpecifications($user);

            if (!$isValid) {
                throw UserInvalidException::build($this->specificationChain->getFailedResultsAsString());
            }
        }
    }
}
