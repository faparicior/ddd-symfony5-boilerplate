<?php declare(strict_types=1);

namespace App\Users\User\Ui\Consumers;

use App\Shared\Application\Subscribers\DomainEventSubscriber;
use App\Users\User\Application\DeleteUser\DeleteUserCommand;
use App\Users\User\Domain\Events\UserWasArchived;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class WhenUserWasArchivedDeleteUser extends DomainEventSubscriber
{
    private LoggerInterface $logger;
    private MessageBusInterface $commandBus;

    public function __construct(MessageBusInterface $commandBus, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->commandBus = $commandBus;
    }

    public function __invoke(UserWasArchived $userWasArchived)
    {
        // TODO: IMPLEMENT THIS FEATURE WITH ArchiveUserWasValidatedByUser instead UserWasArchived
        $this->logger->info("SHOULD DELETE USER" . $userWasArchived->userId());
        $deleteUser = DeleteUserCommand::build($userWasArchived->userId()->value());
        $this->commandBus->dispatch($deleteUser);
    }

    protected static function eventsSubscribed(): array
    {
        return [
            UserWasArchived::class
        ];
    }
}
