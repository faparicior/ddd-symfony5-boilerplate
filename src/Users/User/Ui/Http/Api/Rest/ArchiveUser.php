<?php

declare(strict_types=1);

namespace App\Users\User\Ui\Http\Api\Rest;

use App\Shared\Ui\Http\Api\Rest\AppController;
use App\Users\User\Application\ArchiveUser\ArchiveUserCommand;

class ArchiveUser extends AppController
{
    public function handleRequest($data): ?array
    {
        $deleteUser = ArchiveUserCommand::build($data['email']);

        $this->commandBus->dispatch($deleteUser);

        return null;
    }
}
